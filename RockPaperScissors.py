import random

#
# def game():
#     while True:
#         variants = ["rock", "paper", "scissors"]
#         random_variant = random.choice(variants)
#         player_choice = input("Choose rock or paper or scissors or stop to exit the cycle: ")
#         if player_choice == "stop":
#             break
#         elif player_choice not in variants:
#             raise ValueError("Incorrect value")
#         elif player_choice == "rock" and random_variant == "scissors" or player_choice == "paper" and random_variant == "rock" or player_choice == "scissors" and random_variant == "paper":
#             print("You win")
#         elif player_choice == random_variant:
#             return "Draw"
#         else:
#             print("You lost.")
# print(game())

signs = {"paper": ["scissors"],"scissors": ["rock"],"rock": ["paper"]}

class Sign:
    name: str
    weak: list

    def __init__(self, name,weak):
        self.name = name
        self.weak = weak

    def __gt__(self, other):
        return other.name not in self.weak

    def __lt__(self, other):
        return  other.name in self.weak

    def __eq__(self, other):
        return  other.name == self.name


examples = {name: Sign(name, weak) for name, weak in signs.items()}

def game():
    while True:
        random_variant = random.choice(list(examples.values()))
        player_choice = input("Choose rock or paper or scissors or stop to exit the cycle: ")
        player_class = examples[player_choice]
        if player_class == random_variant:
            print("Draw")
        elif player_class > random_variant:
            print("You win")
        else:
            print("You lost")

if __name__ == "__main__":
    game()
